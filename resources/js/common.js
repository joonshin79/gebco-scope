$(function(){

  var wrap = $('[data-wrap]').data('wrap');
  var page = $('[data-page]').data('page');
  var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi
  var nav1Depth = $('nav>ul>li>a');
  $('#container').addClass(wrap.replace(/ /g, '').toLowerCase());
  if(page == null || page == 'undefined'){
    $('.tit>ul').append('<li><strong>' + wrap +'</strong></li>');
  } else{
    $('.tit>ul').append('<li>' + wrap +'</li>');
    $('.tit>ul').append('<li><strong>' + page +'</strong></li>');
    $('#container>.content').addClass(page.replace(regExp, "").replace(/ /g, '').toLowerCase());
  }

  // wrap 값을 받아와 Nav Active 추가
  navActive = function(){
    if($('nav>ul>li>.active').length == 0){
      nav1Depth.each(function(index, item){
        var text = $(this).text();
        if(wrap == text){
          $(this).addClass('active');
        }
      });
    }
  }
  navActive();

  // Nav Start
  nav1Depth.click(function() {
    var navDiv = $(this).next('div');
    if(navDiv.length > 0){
      if(navDiv.css('display') == 'none'){
        $('nav>ul>li>div').hide();
        nav1Depth.removeClass('active');
        $(this).addClass('active');
        navDiv.slideDown(100);
      } else{
        navDiv.slideUp(100);
        $(this).removeClass('active');
        setTimeout(function() {navActive()}, 700);
      }
      return false;
    }
  });
  nav1Depth.mouseover(function() {
    var navDiv = $(this).next('div');
    $('nav>ul>li>div').hide();
    nav1Depth.removeClass('active');
    $(this).addClass('active');
    navDiv.slideDown(100);
  });
  $('nav>ul>li').mouseleave(function() {
    $(this).find('>div').slideUp(100);
    nav1Depth.removeClass('active');
    setTimeout(function() {navActive()}, 700);
  });
  // Nav End


  // Popup
  $('[data-popup-link]').click(function() {
    var popupLink = $(this).data('popup-link');
    var popup = $('[data-popup-name]'); 
    popup.each(function(index, item){
      var popupName = $(this).data('popup-name');
      if(popupLink == popupName){
        var width = $(this).width() + 100;
        var height = $(this).height() + 100;
        popup.removeClass('active');
        $(this).addClass('active').css({'margin-left': -width/2 });
        if($(".dim-mask").length == 0){
          $('body').append("<div class=\"dim-mask\"></div>");
        }
        return false;
      }
    })
    $('.popup>button').click(function() {
      $(this).parents('[data-popup-name]').removeClass('active');
      $(".dim-mask").remove();
      return false;
    });
  });

  // Search
  var _this = $('.search');
  if(_this.length !== 0){
    var h4 = _this.find('>h4');
    var type = _this.find('>.type');
    h4.click(function(e) {
      e.preventDefault();
      h4.addClass('default').removeClass('active');
      $(this).addClass('active').removeClass('default');
      type.removeClass('active');
      $(this).next('.type').addClass('active');

      return false;
    });

    var radio = _this.find('input[type=radio]');
    radio.before('<span></span>');
    radio.parents('label').click(function(e) {
      radio.parents('label').removeClass('checked');
      $(this).addClass('checked');
      radio.attr("checked", false);
      $(this).find('>input[type=radio]').attr("checked", true);
    });
  }
  // Radio

  //메인
  if(wrap == 'main'){
    var $slideHead = $(".slide .slide_head > ul > li > a");
    var $slidePrev = $(".slide .slide_nav > .prev");
    var $slideNext = $(".slide .slide_nav > .next");
    $slideHead.on("click",function(e){
      e.preventDefault();
      var tabIdx = $(this).closest("li").index();
      mainSlide(tabIdx);
    });
    $slidePrev.on("click",function(e){
      e.preventDefault();
      var prevIdx = $(this).closest(".slide").find(".slide_head > ul > li.is_active").index()-1;
      mainSlide(prevIdx);
    });
    $slideNext.on("click",function(e){
      e.preventDefault();
      var NextIdx = $(this).closest(".slide").find(".slide_head > ul > li.is_active").index()+1;
      mainSlide(NextIdx);
    });
    function mainSlide(idx){
      var slideWidth = 800;
      var slideLeng = $(".slide .slide_body > ul > li").length;
      $(".slide .slide_head > ul > li").not(":eq("+idx+")").removeClass("is_active");
      $(".slide .slide_head > ul > li:eq("+idx+")").addClass("is_active");
      $(".slide .slide_body > ul").css("width",(slideWidth*slideLeng));
      $(".slide .slide_body > ul").animate({"left":-(slideWidth*idx)},200);
      if(idx == 0){
        $(".slide .slide_nav > .prev").css("display","none");
        $(".slide .slide_nav > .next").css("display","block");
      }else if(idx == (slideLeng-1)){
        $(".slide .slide_nav > .prev").css("display","block");
        $(".slide .slide_nav > .next").css("display","none");
      }else{
        $(".slide .slide_nav > .prev").css("display","block");
        $(".slide .slide_nav > .next").css("display","block");
      }
    }
  }
});
